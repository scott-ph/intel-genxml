/*
 * Copyright © 2020 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

extern crate glob;
extern crate roxmltree;

use glob::glob;
use std::cmp::{max, min};
use std::collections::{BTreeMap, HashSet};
use std::env;
use std::fs;
use std::fs::File;
use std::io;
use std::io::Write;
use std::path::Path;
use std::str::FromStr;

#[derive(Debug, Clone)]
struct GenValue {
    name: String,
    value: String,
}

#[derive(Debug, Clone)]
struct GenField {
    name: String,
    start: i32,
    end: i32,
    typ: String,
    default: Option<String>,
    group: Option<GenGroup>,
    values: Vec<GenValue>,
}

#[derive(Debug, Clone, Copy)]
struct GenGroup {
    count: i32,
    start: i32,
    size: i32,
}

#[derive(Clone)]
struct GenEnum {
    name: String,
    prefix: String,
    values: Vec<GenValue>,
}

#[derive(Clone)]
struct GenStruct {
    name: String,
    length: i32,
    num: Option<String>,
    fields: Vec<GenField>,
}

#[derive(Clone, Default)]
struct GenXml {
    name: String,
    gen: i32,
    enums: Vec<GenEnum>,
    structs: BTreeMap<String, GenStruct>,
}

fn camelize(input: &str) -> String {
    let mut input = input.to_string();
    if input.starts_with("3D") {
        input.replace_range(..2, "3D_");
    }
    let output: String = input
        .chars()
        .map(|c| {
            if c.is_alphanumeric() || c == '_' {
                c
            } else {
                '_'
            }
        })
        .collect::<String>()
        .split('_')
        .map(|f| {
            if f.is_empty() {
                f.to_string()
            } else {
                let (first, rest) = f.split_at(1);
                first.to_uppercase() + &rest.to_lowercase()
            }
        })
        .collect();
    if !output.is_empty() && output.chars().next().unwrap().is_ascii_digit() {
        "_".to_string() + &output
    } else {
        output
    }
}

fn snakeize(input: &str) -> String {
    let output = input
        .chars()
        .map(|c| if c.is_alphanumeric() { c } else { '_' })
        .collect::<String>()
        .split('_')
        .filter(|f| !f.is_empty())
        .collect::<Vec<&str>>()
        .join("_")
        .to_lowercase();
    if !output.is_empty() && output.chars().next().unwrap().is_ascii_digit() {
        "_".to_string() + &output
    } else {
        output
    }
}

fn floatize(input: &str) -> String {
    if input.contains('.') {
        input.to_string()
    } else {
        input.to_string() + ".0"
    }
}

fn rust_type(f: &GenField) -> String {
    let size = f.end - f.start + 1;
    if f.typ == "address" {
        "A".to_string()
    } else if f.typ == "offset" {
        "u64".to_string()
    } else if f.typ == "bool" {
        "bool".to_string()
    } else if f.typ == "float" {
        "f32".to_string()
    } else if f.typ == "uint" && size > 32 {
        "u64".to_string()
    } else if f.typ == "int" {
        "i32".to_string()
    } else if f.typ == "uint" {
        "u32".to_string()
    } else if f.typ == "mbo" {
        "mbo".to_string()
    } else if (f.typ.starts_with('s') || f.typ.starts_with('u')) && f.typ.contains('.') {
        "f32".to_string()
    } else if f.typ == "u8" {
        "u8".to_string()
    } else {
        camelize(&f.typ)
    }
}

fn is_struct(x: &GenXml, f: &GenField) -> bool {
    x.structs.contains_key(&rust_type(f))
}

fn is_address(f: &GenField) -> bool {
    rust_type(f) == "A"
}

fn is_fixed(f: &GenField) -> bool {
    f.typ != "float" && rust_type(f) == "f32"
}

fn is_fixed_signed(f: &GenField) -> bool {
    f.typ.starts_with('s')
}

fn frac_bits(f: &GenField) -> Option<i32> {
    f.typ.split('.').nth(1).and_then(|f| f.parse().ok())
}

fn dword_length(s: &GenStruct) -> i32 {
    let mut maxbit: i32 = -1;
    for f in s.fields.iter() {
        match f.group {
            Some(GenGroup { count, start, size }) => {
                maxbit = max(maxbit, start + count * size - 1);
            }
            _ => {
                maxbit = max(maxbit, f.end);
            }
        }
    }
    maxbit / 32 + 1
}

fn attr(xml: &roxmltree::Node, attribute: &str) -> Result<String, String> {
    xml.attribute(attribute)
        .map(|v| v.to_string())
        .ok_or(format!("Node {:?} missing attribute {}", xml, attribute))
}

fn attr_parse<F: FromStr>(xml: &roxmltree::Node, attribute: &str) -> Result<F, String> {
    xml.attribute(attribute)
        .ok_or(format!("Node {:?} missing attribute {}", xml, attribute))
        .and_then(|v| {
            v.parse().ok().ok_or(format!(
                "Node {:?} failed to parse attribute {}",
                xml, attribute
            ))
        })
}

fn parse_one_value(xml: &roxmltree::Node) -> Result<GenValue, String> {
    Ok(GenValue {
        name: attr(xml, "name")?,
        value: attr(xml, "value")?,
    })
}

fn parse_one_field(xml: &roxmltree::Node, group: &Option<GenGroup>) -> Result<GenField, String> {
    let mut e = GenField {
        name: snakeize(&attr(xml, "name").unwrap_or_else(|_| "".to_string())),
        start: attr_parse(xml, "start")?,
        end: attr_parse(xml, "end")?,
        typ: attr(xml, "type")?,
        default: attr(xml, "default").ok(),
        group: *group,
        values: Vec::new(),
    };
    for child in xml.children().filter(|c| c.is_element()) {
        if !child.has_tag_name("value") {
            return Err(format!("Unexpected element {:?}", child));
        }
        e.values.push(parse_one_value(&child)?);
    }
    Ok(e)
}

fn parse_one_group(xml: &roxmltree::Node) -> Result<Vec<GenField>, String> {
    let group = GenGroup {
        count: attr_parse(xml, "count")?,
        start: attr_parse(xml, "start")?,
        size: attr_parse(xml, "size")?,
    };
    let mut tmp = Vec::new();
    if group.count == 0 {
        return Ok(tmp);
    }
    for child in xml.children().filter(|c| c.is_element()) {
        if child.has_tag_name("field") {
            let field = parse_one_field(&child, &Some(group))?;
            let end = min(field.end, group.size - 1);
            if field.start < group.size {
                tmp.push(GenField { end, ..field });
            }
        } else if child.has_tag_name("group") {
            let children = parse_one_group(&child)?;
            tmp.extend(children.into_iter().map(|f| match f.group {
                Some(nested_group) => GenField {
                    group: Some(GenGroup {
                        count: group.count * nested_group.count,
                        start: group.start,
                        size: nested_group.size,
                    }),
                    ..f
                },
                None => f,
            }));
        } else {
            return Err(format!("Unexpected element {:?}", child));
        }
    }
    Ok(tmp)
}

fn parse_one_enum(xml: &roxmltree::Node) -> Result<GenEnum, String> {
    let mut e = GenEnum {
        name: camelize(&attr(xml, "name")?),
        prefix: attr(xml, "name").unwrap_or_else(|_| "".to_string()),
        values: Vec::new(),
    };
    for child in xml.children().filter(|c| c.is_element()) {
        if !child.has_tag_name("value") {
            return Err(format!("Unexpected element {:?}", child));
        }
        e.values.push(GenValue {
            name: attr(&child, "name")?,
            value: attr(&child, "value")?,
        })
    }
    Ok(e)
}

fn parse_one_struct(xml: &roxmltree::Node) -> Result<GenStruct, String> {
    let mut s = GenStruct {
        name: camelize(&attr(xml, "name")?),
        length: attr_parse(xml, "length").unwrap_or(0),
        num: attr(xml, "num").map(|s| s.to_lowercase()).ok(),
        fields: Vec::new(),
    };
    for child in xml.children().filter(|c| c.is_element()) {
        if child.has_tag_name("field") {
            s.fields.push(parse_one_field(&child, &None)?);
        } else if child.has_tag_name("group") {
            s.fields.extend(parse_one_group(&child)?);
        } else {
            return Err(format!("Unexpected element {:?}", child));
        }
    }
    Ok(GenStruct {
        length: max(s.length, dword_length(&s)),
        ..s
    })
}

fn parse_one(xml: &roxmltree::Document) -> Result<GenXml, String> {
    let genxml = xml.root_element();
    if !genxml.has_tag_name("genxml") {
        return Err(format!("Unexpected element {:?}", genxml));
    }
    let mut gen = GenXml {
        name: attr(&genxml, "name")?,
        gen: attr(&genxml, "gen")?
            .replace(".", "")
            .parse()
            .map_err(|_e| "Failed to parse gen")?,
        ..Default::default()
    };

    for child in genxml.children().filter(|c| c.is_element()) {
        if child.has_tag_name("enum") {
            gen.enums.push(parse_one_enum(&child)?);
        } else if child.has_tag_name("struct")
            || child.has_tag_name("instruction")
            || child.has_tag_name("register")
        {
            let s = parse_one_struct(&child)?;
            gen.structs.insert(s.name.to_string(), s);
        } else {
            return Err(format!("Unknown element {:?}", child));
        }
    }
    Ok(gen)
}

fn handle_large_buffers(gen: &GenXml) -> GenXml {
    GenXml {
        structs: gen
            .structs
            .iter()
            .map(|(name, strct)| {
                (
                    name.to_owned(),
                    GenStruct {
                        fields: strct
                            .fields
                            .iter()
                            .map(|field| {
                                let size = field.end - field.start + 1;
                                if size > 64 && size % 8 == 0 && field.typ == "uint" {
                                    let (reps, start) = match field.group {
                                        Some(g) => (g.count, g.start),
                                        None => (1, field.start),
                                    };
                                    GenField {
                                        group: Some(GenGroup {
                                            count: size / 8 * reps,
                                            start,
                                            size: 8,
                                        }),
                                        start: 0,
                                        end: 7,
                                        typ: "u8".to_string(),
                                        ..field.to_owned()
                                    }
                                } else {
                                    field.to_owned()
                                }
                            })
                            .collect(),
                        ..strct.to_owned()
                    },
                )
            })
            .collect(),
        ..gen.to_owned()
    }
}

fn emit_enum(e: &GenEnum, out: &mut File) -> io::Result<()> {
    writeln!(out, "    #[derive(Clone, Copy)]")?;
    writeln!(out, "    pub enum {} {{", e.name)?;
    for v in e.values.iter() {
        let name = camelize(&format!("{}_{}", e.prefix, &v.name));
        writeln!(out, "        {},", name)?;
    }
    writeln!(out, "        Unknown(u32),")?;
    writeln!(out, "    }}")?;
    writeln!(out, "    impl Into<u32> for {} {{", e.name)?;
    writeln!(out, "        fn into(self) -> u32 {{")?;
    writeln!(out, "            type E = {};", e.name)?;
    writeln!(out, "            match self {{")?;
    for v in e.values.iter() {
        let name = camelize(&format!("{}_{}", e.prefix, &v.name));
        writeln!(out, "                E::{} => {},", name, v.value)?;
    }
    writeln!(out, "                E::Unknown(val) => val,")?;
    writeln!(out, "            }}")?;
    writeln!(out, "        }}")?;
    writeln!(out, "    }}")?;
    writeln!(out, "    impl From<u32> for {} {{", e.name)?;
    writeln!(out, "        fn from(val: u32) -> Self {{")?;
    writeln!(out, "            match val {{")?;
    for v in e.values.iter() {
        let name = camelize(&format!("{}_{}", e.prefix, &v.name));
        writeln!(out, "                {} => Self::{},", v.value, name)?;
    }
    writeln!(out, "                _ => Self::Unknown(val),")?;
    writeln!(out, "            }}")?;
    writeln!(out, "        }}")?;
    writeln!(out, "    }}")?;
    writeln!(out, "    impl Default for {} {{", e.name)?;
    writeln!(out, "        fn default() -> Self {{")?;
    writeln!(out, "            0u32.into()")?;
    writeln!(out, "        }}")?;
    writeln!(out, "    }}")?;

    Ok(())
}

#[derive(Clone, Debug)]
struct FieldInfo<'a> {
    field: &'a GenField,
    index: Option<i32>,
    start_bit: i32,
    end_bit: i32,
    start_dword: i32,
    end_dword: i32,
}

fn group_fields<'a>(gen: &GenXml, s: &'a GenStruct) -> Vec<Vec<FieldInfo<'a>>> {
    let mut res: Vec<Vec<FieldInfo<'a>>> = Vec::with_capacity(s.length as usize);
    res.resize_with(s.length as usize, Default::default);
    let mut fields = BTreeMap::new();
    for f in s.fields.iter() {
        match f.group {
            Some(GenGroup { count, start, size }) => {
                for i in 0..count {
                    let start_bit = i * size + start + f.start;
                    let end_bit = i * size + start + f.end;
                    fields.insert(
                        start_bit,
                        FieldInfo {
                            field: f,
                            index: Some(i),
                            start_bit,
                            end_bit,
                            start_dword: start_bit / 32,
                            end_dword: end_bit / 32,
                        },
                    );
                }
            }
            None => {
                fields.insert(
                    f.start,
                    FieldInfo {
                        field: f,
                        index: None,
                        start_bit: f.start,
                        end_bit: f.end,
                        start_dword: f.start / 32,
                        end_dword: f.end / 32,
                    },
                );
            }
        }
    }
    for (_, mut f) in fields {
        if is_struct(gen, &f.field) {
            let end_dword = gen.structs[&rust_type(&f.field)].length + f.start_dword - 1;
            let end_bit = min(f.end_bit, (end_dword + 1) * 32 - 1);
            f = FieldInfo {
                end_dword,
                end_bit,
                ..f
            };
        }
        for d in f.start_dword..f.end_dword + 1 {
            res[d as usize].push(f.clone());
        }
    }
    res
}

fn destructure_binder(
    name_prefix: &str,
    start_dword: i32,
    end_dword: i32,
    length: i32,
    is_mut: bool,
) -> (String, String) {
    let binder_name = if start_dword == end_dword {
        format!("{}{}", name_prefix, start_dword)
    } else {
        format!("{}{}_{}", name_prefix, start_dword, end_dword)
    };

    let mut binder = "let [".to_string();
    for _ in 0..start_dword {
        binder += "_, ";
    }
    binder += &format!(
        "ref {}{} @ ..",
        if is_mut { "mut " } else { "" },
        binder_name
    );
    for _ in end_dword + 1..length {
        binder += ", _";
    }
    binder += "]";
    (binder_name, binder)
}

fn emit_consts(s: &GenStruct, out: &mut File) -> io::Result<()> {
    let mut consts = BTreeMap::new();
    let mut conflict = HashSet::new();

    writeln!(out, "        pub const DWORD_LENGTH: u32 = {};", s.length)?;
    if let Some(num) = &s.num {
        writeln!(out, "        pub const NUM: u64 = {};", num)?;
    }
    for f in s.fields.iter() {
        for GenValue { name, value } in f.values.iter() {
            let name = snakeize(name).to_uppercase();
            let mut value = match rust_type(&f).as_str() {
                "f32" => floatize(value),
                _ => value.to_string(),
            };
            value = rust_type(&f) + " = " + &value;
            if consts.contains_key(&name) {
                if consts[&name] != value {
                    conflict.insert(name);
                }
                continue;
            }
            consts.insert(name, value);
        }
    }
    for (name, value) in consts {
        if conflict.contains(&name) {
            continue;
        }
        writeln!(out, "        pub const {}: {};", name, value)?;
    }
    Ok(())
}

fn emit_default(s: &GenStruct, type_param: (&str, &str), out: &mut File) -> io::Result<()> {
    writeln!(
        out,
        "    impl{} Default for {}{} {{",
        type_param.0, s.name, type_param.1
    )?;
    writeln!(out, "        fn default() -> Self {{")?;
    writeln!(out, "            {} {{", s.name)?;
    for f in s.fields.iter() {
        if f.typ == "mbo" {
            continue;
        }

        let val = if let Some(default) = &f.default {
            match rust_type(&f).as_str() {
                "bool" => {
                    if default == "0" || default == "false" {
                        "false".to_string()
                    } else {
                        "true".to_string()
                    }
                }
                "f32" => floatize(default),
                _ => default.to_string(),
            }
        } else if let Some(group) = f.group {
            if group.count > 32 {
                format!("[0; {}]", group.count)
            } else {
                "Default::default()".to_string()
            }
        } else {
            "Default::default()".to_string()
        };
        writeln!(out, "                {}: {},", f.name, val)?;
    }
    writeln!(out, "            }}")?;
    writeln!(out, "        }}")?;
    writeln!(out, "    }}")?;
    Ok(())
}

fn emit_pack(
    gen: &GenXml,
    s: &GenStruct,
    type_param: (&str, &str),
    out: &mut File,
) -> io::Result<()> {
    let dwords = group_fields(gen, s);

    writeln!(
        out,
        "    impl{} Serialize for {}{} {{",
        type_param.0, s.name, type_param.1
    )?;
    writeln!(out, "        type Out = [u32; {}];", s.length)?;

    writeln!(
        out,
        "        fn pack_into(&self, out: &mut [u32; {}]) {{",
        s.length
    )?;

    for (n, dword) in dwords.iter().enumerate() {
        let n = n as i32;
        let has_address = dword
            .iter()
            .any(|i| is_address(&i.field) && i.start_dword == n);
        if dword.is_empty() {
            writeln!(out, "            out[{}] = 0;", n)?;
        } else if dword
            .iter()
            .any(|i| is_address(&i.field) && i.start_dword != n)
        {
            continue;
        } else if dword.len() == 1
            && is_struct(gen, dword[0].field)
            && dword[0].start_bit == dword[0].start_dword * 32
        {
            if dword[0].start_dword != n {
                continue;
            }

            let FieldInfo {
                field,
                index,
                start_bit: _,
                end_bit: _,
                start_dword,
                end_dword,
            } = dword[0];
            writeln!(out, "            {{")?;
            let (binder_name, binder) =
                destructure_binder("out", start_dword, end_dword, s.length, true);
            writeln!(out, "                {} = out;", binder)?;
            writeln!(
                out,
                "                self.{}{}.pack_into({});",
                field.name,
                index
                    .map(|i| format!("[{}]", i))
                    .unwrap_or_else(|| "".to_string()),
                binder_name
            )?;
            writeln!(out, "            }}")?;
        } else {
            for (m, i) in dword
                .iter()
                .filter(|i| is_struct(gen, &i.field) && i.start_dword == n)
                .enumerate()
            {
                let index = i
                    .index
                    .map(|i| format!("[{}]", i))
                    .unwrap_or_else(|| "".to_string());
                writeln!(out, "            let mut v{}_{} = [0_u32; 1];", n, m)?;
                writeln!(
                    out,
                    "            self.{}{}.pack_into(&mut v{}_{});",
                    i.field.name, index, n, m
                )?;
            }
            let mut v = 0;
            let vals: Vec<String> = dword
                .iter()
                .filter(|i| {
                    !is_address(&i.field) && (!is_struct(gen, &i.field) || i.start_dword == n)
                })
                .map(|i| {
                    let index = i
                        .index
                        .map(|i| format!("[{}]", i))
                        .unwrap_or_else(|| "".to_string());
                    let start = max(i.start_bit - n * 32, 0);
                    let end = min(i.end_bit - n * 32, 31);
                    if is_struct(gen, &i.field) {
                        let m = v;
                        v += 1;
                        format!("__gen_uint(v{}_{}[0], {}, {})", n, m, start, end)
                    } else if i.field.typ == "mbo" {
                        format!("__gen_mbo({}, {})", start, end)
                    } else if i.field.typ == "float" {
                        format!("self.{}{}.to_bits()", i.field.name, index,)
                    } else if is_fixed(&i.field) {
                        let func = if is_fixed_signed(&i.field) { "s" } else { "u" };
                        format!(
                            "__gen_{}fixed(self.{}{}, {}, {}, {})",
                            func,
                            i.field.name,
                            index,
                            start,
                            end,
                            frac_bits(&i.field).unwrap()
                        )
                    } else {
                        let func = if i.field.typ == "offset" {
                            "offset"
                        } else {
                            "uint"
                        };
                        let convert = match rust_type(&i.field).as_str() {
                            "u32" => "",
                            "u64" => " as u32",
                            "i32" => " as u32",
                            _ => ".into()",
                        };
                        if n == i.start_dword {
                            format!(
                                "__gen_{}(self.{}{}{}, {}, {})",
                                func, i.field.name, index, convert, start, end
                            )
                        } else {
                            format!(
                                "__gen_{}((self.{}{} >> {}) as u32, {}, {})",
                                func,
                                i.field.name,
                                index,
                                (n - i.start_dword) * 32,
                                start,
                                end
                            )
                        }
                    }
                })
                .collect();
            if has_address {
                let i = dword.iter().find(|i| is_address(&i.field)).unwrap();
                let index = i
                    .index
                    .map(|i| format!("[{}]", i))
                    .unwrap_or_else(|| "".to_string());
                let val = if vals.is_empty() {
                    "0".to_string()
                } else {
                    writeln!(
                        out,
                        "            let v{} = {};",
                        n,
                        vals.join(" |\n                ")
                    )?;
                    format!("v{}", n)
                };
                writeln!(
                    out,
                    "            let v{}_address = self.{}{}.combine({});",
                    n, i.field.name, index, val
                )?;
                writeln!(out, "            out[{}] = v{}_address as u32;", n, n)?;
                if dwords.len() as i32 >= n + 2 {
                    writeln!(
                        out,
                        "            out[{}] = (v{}_address >> 32) as u32;",
                        n + 1,
                        n
                    )?;
                }
            } else {
                writeln!(
                    out,
                    "            out[{}] = {};",
                    n,
                    vals.join(" |\n                ")
                )?;
            }
        }
    }
    writeln!(out, "        }}")?;
    writeln!(
        out,
        "        fn write_to<W: std::io::Write>(&self, write: &mut W) -> std::io::Result<()> {{"
    )?;
    writeln!(out, "            let mut out = [0_u32; {}];", s.length)?;
    writeln!(out, "            self.pack_into(&mut out);")?;
    writeln!(
        out,
        "            write.write_all(unsafe {{ &*(&out as *const [u32; {}] as *const [u8; {}]) }})",
        s.length,
        s.length * 4
    )?;
    writeln!(out, "        }}")?;
    writeln!(out, "    }}")?;

    Ok(())
}

fn unpack_one(gen: &GenXml, s: &GenStruct, f: &GenField) -> String {
    if f.group.is_some() {
        let group = f.group.unwrap();
        let vals: Vec<String> = (0..group.count)
            .map(|i| {
                let start = group.start + group.size * i;
                let fprime = GenField {
                    start: start + f.start,
                    end: start + f.end,
                    group: None,
                    ..f.clone()
                };
                unpack_one(gen, s, &fprime)
            })
            .collect();
        return format!(
            "[\n{}\n                ]",
            vals.join(",\n                    ")
        );
    }

    if is_struct(gen, &f) && f.start % 32 == 0 {
        let end_dword = gen.structs[&rust_type(&f)].length + f.start / 32 - 1;
        let (binder_name, binder) =
            destructure_binder("in", f.start / 32, end_dword, s.length, false);
        let mut val = "{{\n".to_string();
        val += &format!("                    {} = input;\n", binder);
        val += &format!("                    {}.into()\n", binder_name);
        val += "                }}";
        val
    } else if is_struct(gen, &f) {
        let mut val = "{{\n".to_string();
        val += &format!(
            "                    let i: [u32; 1] = [__gen_unuint(input[{}], {}, {})];\n",
            f.start / 32,
            f.start % 32,
            f.end % 32
        );
        val += "                    (&i).into()\n";
        val += "                }}";
        val
    } else {
        let vals: Vec<String> = (f.start / 32..f.end / 32 + 1)
            .map(|dword| {
                let is_address = f.typ == "address" || f.typ == "offset";
                let start = max(f.start - dword * 32, 0);
                let end = min(f.end - dword * 32, 31);
                let shift =
                    max(dword * 32 - f.start, 0) + if is_address { f.start % 32 } else { 0 };
                let shiftstr = if shift > 0 {
                    format!(" << {}", shift)
                } else {
                    "".to_string()
                };
                let cast = f.end - f.start >= 32 || is_address;
                if f.typ == "bool" {
                    format!("__gen_unuint(input[{}], {}, {}) != 0", dword, start, end)
                } else if f.typ == "float" {
                    format!("f32::from_bits(input[{}])", dword)
                } else if is_fixed(&f) {
                    let func = if is_fixed_signed(&f) { "s" } else { "u" };
                    format!(
                        "__gen_un{}fixed(input[{}], {}, {}, {})",
                        func,
                        dword,
                        start,
                        end,
                        frac_bits(&f).unwrap()
                    )
                } else if cast {
                    format!(
                        "(__gen_unuint(input[{}], {}, {}) as u64){}",
                        dword, start, end, shiftstr
                    )
                } else {
                    format!(
                        "__gen_unuint(input[{}], {}, {}){}",
                        dword, start, end, shiftstr
                    )
                }
            })
            .collect();
        let val = vals.join(" |\n                    ");
        match rust_type(&f).as_str() {
            "bool" | "f32" | "u32" | "u64" | "A" => val,
            t @ "i32" | t @ "u8" => format!("({}) as {}", val, t),
            _ => format!("({}).into()", val),
        }
    }
}

fn emit_unpack(
    gen: &GenXml,
    s: &GenStruct,
    type_param: (&str, &str),
    out: &mut File,
) -> io::Result<()> {
    writeln!(
        out,
        "    impl{} From<&[u32; {}]> for {}{} {{",
        type_param.0, s.length, s.name, type_param.1
    )?;
    writeln!(
        out,
        "        fn from({}input: &[u32; {}]) -> Self {{",
        if s.fields.is_empty() { "_" } else { "" },
        s.length
    )?;

    writeln!(out, "            {} {{", s.name)?;
    for f in s.fields.iter() {
        if f.typ == "mbo" {
            continue;
        }
        let val = unpack_one(gen, s, &f);
        writeln!(out, "                {}: {},", f.name, val)?;
    }
    writeln!(out, "            }}")?;
    writeln!(out, "        }}")?;
    writeln!(out, "    }}")?;

    writeln!(
        out,
        "    impl{} Deserialize for {}{} {{",
        type_param.0, s.name, type_param.1
    )?;
    writeln!(
        out,
        "        fn read_from<R: std::io::Read>(read: &mut R) -> std::io::Result<Self> {{"
    )?;
    writeln!(out, "            let mut input = [0_u32; {}];", s.length)?;
    writeln!(out, "            read.read_exact(unsafe {{ &mut *(&mut input as *mut [u32; {}] as *mut [u8; {}]) }})?;", s.length, s.length * 4)?;
    writeln!(out, "            Ok((&input).into())",)?;
    writeln!(out, "        }}")?;
    writeln!(out, "    }}")?;

    Ok(())
}

fn emit_struct(
    gen: &GenXml,
    s: &GenStruct,
    addr_users: &HashSet<String>,
    out: &mut File,
) -> io::Result<()> {
    let uses_addr = addr_users.contains(&s.name);
    let type_param = if uses_addr {
        ("<A: Addr + Default>", "<A>")
    } else {
        ("", "")
    };
    writeln!(out, "    pub struct {}{} {{", s.name, type_param.0)?;
    for f in s.fields.iter() {
        let mut typ = rust_type(&f);
        if typ == "mbo" {
            continue;
        }
        if addr_users.contains(&typ) {
            typ += type_param.1;
        };
        if let Some(group) = f.group {
            typ = format!("[{}; {}]", typ, group.count);
        }
        writeln!(out, "        pub {}: {},", &f.name, typ)?;
    }
    writeln!(out, "    }}")?;
    writeln!(
        out,
        "    impl{} {}{} {{",
        type_param.0, s.name, type_param.1
    )?;
    emit_consts(s, out)?;
    writeln!(out, "    }}")?;

    emit_default(s, type_param, out)?;

    emit_pack(gen, s, type_param, out)?;

    let type_param = if uses_addr { ("", "<u64>") } else { ("", "") };
    emit_unpack(gen, s, type_param, out)?;

    Ok(())
}

fn struct_addr_users(gen: &GenXml) -> HashSet<String> {
    let mut addr_users: HashSet<String> = gen
        .structs
        .iter()
        .filter(|(_, s)| s.fields.iter().any(|f| f.typ == "address"))
        .map(|(name, _)| name.to_string())
        .collect();
    let mut last_len = 0;
    while addr_users.len() != last_len {
        last_len = addr_users.len();
        for (_, s) in gen.structs.iter() {
            for f in s.fields.iter() {
                if addr_users.contains(&rust_type(&f)) {
                    addr_users.insert(s.name.to_string());
                }
            }
        }
    }
    addr_users
}

fn emit_one(gen: &GenXml, out: &mut File) -> io::Result<()> {
    writeln!(out, "pub mod gen{} {{ // {}", gen.gen, gen.name)?;
    writeln!(out, "    use crate::*;")?;
    for e in gen.enums.iter() {
        emit_enum(&e, out)?;
    }
    let addr_users: HashSet<String> = struct_addr_users(&gen);
    for (_, s) in gen.structs.iter() {
        emit_struct(&gen, &s, &addr_users, out)?;
    }
    writeln!(out, "}} // {}", gen.name)?;
    Ok(())
}

fn do_one(xml_filename: &str, out: &mut File) -> Result<(), String> {
    let contents = fs::read_to_string(xml_filename)
        .map_err(|_e| format!("failed to read {}", xml_filename))?;
    let xml = roxmltree::Document::parse(&contents)
        .map_err(|_e| format!("failed to parse {}", xml_filename))?;
    let gen = parse_one(&xml)?;
    let gen = handle_large_buffers(&gen);
    let mut out_one =
        File::create(Path::new(&env::var("OUT_DIR").unwrap()).join(format!("gen{}.rs", gen.gen)))
            .map_err(|_| "could not create gen file")?;
    emit_one(&gen, &mut out_one)
        .map_err(|_e| format!("failed to write rs for {}", xml_filename))?;
    writeln!(
        out,
        "{}",
        format!(
            r#"include!(concat!(env!("OUT_DIR"), "/gen{}.rs"));"#,
            gen.gen
        )
    )
    .map_err(|_| "write failed")?;
    Ok(())
}

fn main() {
    let mut out =
        File::create(Path::new(&env::var("OUT_DIR").unwrap()).join("generated.rs")).unwrap();
    for x in glob("**/gen*xml").unwrap() {
        let x = x.unwrap();
        println!("cargo:rerun-if-changed={}", x.to_str().unwrap());
        do_one(&x.to_str().unwrap(), &mut out).unwrap();
    }
}
