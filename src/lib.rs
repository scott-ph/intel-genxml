/*
 * Copyright © 2020 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#[inline]
fn __gen_ufixed(data: f32, start: u32, end: u32, frac_bits: u32) -> u32 {
    let shift = (frac_bits as f32).exp2();
    let val = (data * shift).round() as u32;
    (val << start) & (u32::MAX >> (31 - end))
}

#[inline]
fn __gen_unufixed(data: u32, start: u32, end: u32, frac_bits: u32) -> f32 {
    let shift = (frac_bits as f32).exp2();
    let val = (data >> start) & (u32::MAX >> (31 - start - end));
    (val as f32) / shift
}

#[inline]
fn __gen_sfixed(data: f32, start: u32, end: u32, frac_bits: u32) -> u32 {
    let shift = (frac_bits as f32).exp2();
    let val = (data * shift).round() as i32;
    (val << start) as u32 & (u32::MAX >> (31 - end))
}

#[inline]
fn __gen_unsfixed(data: u32, start: u32, end: u32, frac_bits: u32) -> f32 {
    let shift = (frac_bits as f32).exp2();
    let val = (data << (31 - end)) as i32 >> (31 - end + start);
    (val as f32) / shift
}

#[inline]
fn __gen_uint(data: u32, start: u32, _end: u32) -> u32 {
    data << start
}

#[inline]
fn __gen_unuint(data: u32, start: u32, end: u32) -> u32 {
    (data >> start) & (u32::MAX >> (31 - start - end))
}

#[inline]
fn __gen_offset(data: u32, _start: u32, _end: u32) -> u32 {
    data
}

#[inline]
fn __gen_mbo(start: u32, end: u32) -> u32 {
    (u32::MAX << start) & (u32::MAX >> (31 - end))
}

pub trait Serialize {
    type Out;
    fn pack_into(&self, out: &mut Self::Out);
    fn write_to<W: std::io::Write>(&self, write: &mut W) -> std::io::Result<()>;
}

pub trait Deserialize {
    fn read_from<R: std::io::Read>(read: &mut R) -> std::io::Result<Self>
    where
        Self: Sized;
}

pub trait Addr {
    fn combine(&self, delta: u32) -> u64;
}

impl Addr for u64 {
    fn combine(&self, delta: u32) -> u64 {
        self + delta as u64
    }
}

include!(concat!(env!("OUT_DIR"), "/generated.rs"));
